/** @type {import('tailwindcss').Config} */
const withMT = require("@material-tailwind/react/utils/withMT");
module.exports = withMT({
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "path-to-your-node_modules/@material-tailwind/react/components/**/*.{js,ts,jsx,tsx}",
    "path-to-your-node_modules/@material-tailwind/react/theme/components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      
      borderRadius: { 
        backgroundLogin: "30% 70% 70% 30% / 30% 30% 70% 70%",

      },
      backgroundColor: {
        white: "#ffffff",
      },
      backgroundImage: {
        backgroundLogin: "url(/assets/login_background.jpg)"
      },
      colors: {
        'blue-light': "#0DD0DA",
      },
    },
  },
  plugins: [],
});
