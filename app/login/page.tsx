"use client";
import React from "react";
import Image from "next/image";

import {
  Card,
  Checkbox,
  Input,
  Button,
  Typography,
  IconButton,
} from "@material-tailwind/react";

import PersonIcon from "@mui/icons-material/Person";
import LockRoundedIcon from "@mui/icons-material/LockRounded";
import GoogleIcon from "@mui/icons-material/Google";
import TwitterIcon from "@mui/icons-material/Twitter";
import FacebookIcon from "@mui/icons-material/Facebook";

export default function Login() {
  return (
    <>
      <div className="flex h-full">
        {/* --Login Left */}
        <div className="w-[50%] h-[617px] flex justify-center items-center">
          <div className="bg-backgroundLogin bg-cover rounded-r-[20px] w-full h-full"></div>
        </div>
        {/* --Login Right */}
        <div className="w-[50%] h-[617px]  flex  justify-center items-center">
          <Card color="transparent" className="" shadow={false}>
            <Typography
              variant="h1"
              color="black"
              className="font-bold text-[40px]"
            >
              Welcome To SunCV
            </Typography>
            {/* Form Login */}
            <form className="mt-8 mb-2 w-full">
              {/* Email Address */}
              <div className="flex h-[45px] w-[360px]">
                <PersonIcon className="flex items-center mr-4 h-[60px]" />
                <Input
                  size="lg"
                  type="email"
                  placeholder="Email Address"
                  variant="standard"
                  className="w-[360px] h-[35px] mb-[10px]  focus:placeholder-[black]"
                  crossOrigin={undefined}
                />
              </div>
              {/* Password */}
              <div className="flex h-[45px] w-[360px] mt-[30px]">
                <LockRoundedIcon className="flex items-center mr-4 h-[55px]" />
                <Input
                  size="lg"
                  type="password"
                  variant="standard"
                  placeholder="Password"
                  className="w-[360px] h-[35px] outline-0 mb-[10px] focus:placeholder-[black]"
                  crossOrigin={undefined}
                />
              </div>
              {/* Checkbox & Register */}
              <div className=" flex flex-row justify-between mt-4">
                <Checkbox label="Remember me" crossOrigin={undefined} />
                <Typography variant="small" className="mt-[15px]">
                  <a href="#" className="text-[grey] decoration-solid">
                    Create an Account
                  </a>
                </Typography>
              </div>
              {/* Button Login */}
              <Button color="blue" className="mt-3" size="lg">
                Login
              </Button>
              {/* List Media */}
              <div className="flex mt-[50px] gap-6">
                <p>Or login with</p>
                <ul className="flex gap-2">
                  <li>
                    <a href="#">
                      <IconButton className="transform scale-100 hover:scale-125 transition-transform duration-1000 rounded bg-[#3b5998] hover:shadow-[#3b5998]/20 focus:shadow-[#3b5998]/20 active:shadow-[#3b5998]/10">
                        <FacebookIcon />
                      </IconButton>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <IconButton className="transform scale-100 hover:scale-125 transition-transform duration-1000 rounded bg-[#1DA1F2] hover:shadow-[#1DA1F2]/20 focus:shadow-[#1DA1F2]/20 active:shadow-[#1DA1F2]/10">
                        <TwitterIcon />
                      </IconButton>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <IconButton className="transform scale-100 hover:scale-125 transition-transform duration-1000 rounded bg-[#ea4335] hover:shadow-[#ea4335]/20 focus:shadow-[#ea4335]/20 active:shadow-[#ea4335]/10">
                        <GoogleIcon />
                      </IconButton>
                    </a>
                  </li>
                </ul>
              </div>
            </form>
          </Card>
        </div>
      </div>
    </>
  );
}
