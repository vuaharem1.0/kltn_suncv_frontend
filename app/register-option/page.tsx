"use client";

import React from "react";
import Image from "next/image";

import {
  Card,
  Checkbox,
  Input,
  Button,
  Typography,
  IconButton,
} from "@material-tailwind/react";

export default function RegisterOption() {
  return (
    <>
      <div className="w-full h-[617px] flex justify-center items-center">
        <div className=" w-[690px] h-[452px]  border-blue-light border-[1px] rounded-[15px]">
          <Card
            color="transparent"
            className="flex justify-center items-center mt-[50px]"
            shadow={false}
          >
            <Typography variant="h3">
              Join as a Candidate or Employer
            </Typography>
            <div className="flex justify-between mt-[20px] w-[80%] h-[170px] bg-[red] ">
              <a className="w-[40%] h-[158px] bg-[blue] "></a>
              <div className="w-[40%] h-[158px] bg-[blue]"></div>
            </div>
          </Card>
        </div>
      </div>
    </>
  );
}
