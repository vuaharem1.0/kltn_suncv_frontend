import Image from "next/image";

import Login from "./login/page";
import Register from "./register/page";
import RegisterOption from "@/app/register-option/page";

export default function Home() {
  return (
    <>
      <RegisterOption />
    </>
  );
}
